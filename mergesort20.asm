# Merge Sort
.data # Data declaration section
    array: .asciiz "Array: "
    endl: .asciiz "\n"
    space: .asciiz "  "
    left: .asciiz "left:"
    right: .asciiz "right:"
    mid: .asciiz "mid:"
    #input array:
    Array: .word 5 87 43 -23 -12 19 32 -20 291 68 72 -54 -192 26 11 -90 84 -49 22 39
    #temp array:
    TempArray: .word 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    
.text

main:
    addi $sp, $sp, -4 
    sw $ra, 0($sp) 
    la $a0, TempArray # address of temp array
    la $a1, Array # address of primitive array
    addi $a2, $zero, 20 # size
    and $a3, $zero, $zero # initializing left = 0
    move $t0, $a1 
    move $t1, $a2     
    li $v0, 4 
    la $a0, array 
    syscall
    li $t2, 0 # counter variable for printing unsorted array
    
PrintUnsorted: 
    bge $t2, $a2, ExitPrintUnsorted # if i >= length then exit the loop
    sll $t0, $t2, 2
    add $t3, $a1, $t0 
    li $v0, 1
    lw $a0, 0($t3) 
    syscall 

    li $v0, 4
    la $a0, space
    syscall
    addi $t2, $t2, 1 # i ++
    j PrintUnsorted
    
ExitPrintUnsorted:
    li $v0, 4 
    la $a0, endl #print newline
    syscall
    
    addi $sp, $sp, -16 
    sw $ra, 0($sp) # return address
    sw $a1, 8($sp) # save address unsorted array
    add $a2, $a2, -1 # $a2 = right - 1
    sw $a2, 4($sp) 
    sw $a3, 0($sp)
    jal MergeSort # jump to mergesort(left, mid, right)
    
EndProgram:
    addi $sp, $sp, 20 # pop items from stack
    li $v0, 10 
    syscall 
    
MergeSort:
    addi $sp, $sp, -20 
    sw $ra, 16($sp)
    sw $s1, 12($sp) # save address of unsorted array
    sw $s2, 8($sp) # save right 
    sw $s3, 4($sp) # save left
    sw $s4, 0($sp) # save nud
    move $s1, $a1 # move address of array to $s1
    move $s2, $a2 # move right to $s2
    move $s3, $a3 # move left to $s3
    bge $s3, $s2, ExitMergeSort # if left > right
    add $s4, $s3, $s2
    div $s4, $s4, 2 # $s4 = mid
    move $a2, $s4 # argument left
    move $a3, $s3 # argument mid
    jal MergeSort # recursive call for (array, left, mid)
    addi $t4, $s4, 1 # argument mid +1
    move $a3, $t4 # argument left = (mid+1)
    move $a2, $s2 # argument right
    jal MergeSort # recursive call for (a, mid+1, right)
    move $a1, $s1 # Argument array address
    move $a2, $s2 # Argument right
    move $a3, $s3 # Argument left
    move $a0, $s4 # Argument mid
    jal Merge # jump to merge (array, right, left, mid)

ExitMergeSort:
    lw $ra, 16($sp)
    lw $s1, 12($sp) # load aarray address
    lw $s2, 8($sp) # load right
    lw $s3, 4($sp) # loa left
    lw $s4, 0($sp) # load mid
    addi $sp, $sp, 20 # clear the stack
    jr $ra
    
Merge:
    addi $sp, $sp, -20 
    sw $ra, 16($sp) 
    sw $s1, 12($sp) # save array address
    sw $s2, 8($sp) # save arguments size of array = right
    sw $s3, 4($sp) # save left size of array
    sw $s4, 0($sp) # save mid
    move $s1, $a1 # array address
    move $s2, $a2 # move right
    move $s3, $a3 # move left
    move $s4, $a0 # move mid
    move $t1, $s3 # i = left
    move $t3, $a3 # k = left
    addi $t2, $s4, 1 # j= mid + 1
    
while0:
    blt $s4, $t1, while1 # if i>= mid go to while1
    blt $s2, $t2, while1 # if j>= right go to while1
    sll $t6, $t1, 2
    add $t6, $s1, $t6
    lw $s5, 0($t6) # $s5 = a[i]
    sll $t7, $t2, 2
    add $t7, $s1, $t7
    lw $s6, 0($t7) # $s6 = a[j]
    slt $t4, $s5, $s6 
    beq $t4, $zero, else # go to else if a[i] >= a[j}
    sll $t8, $t3, 2
    la $a0, TempArray # load address of temporary array
    add $t8, $a0, $t8 # $t8 = address tepm[k]
    sw $s5, 0($t8) # temp[k] = a[i]
    addi $t3, $t3, 1 # k++
    addi $t1, $t1, 1 # i++
    j while0
else:
    sll $t8, $t3, 2 
    la $a0, TempArray # # load address of temporary array 
    add $t8, $a0, $t8 # $t8 = address temp[k]
    sw $s6, 0($t8) # temp[k] = a[j]
    addi $t3, $t3, 1 # k++
    addi $t2, $t2, 1 # j++
    j while0
while1:
    blt $s4, $t1, while2 # if i >= mid go to while2
    sll $t6, $t1, 2 
    add $t6, $s1, $t6 # $t6 = address a[i]
    lw $s5, 0($t6) # $s5 = a[i]
    sll $t8, $t3, 2
    la $a0, TempArray # load address of temporary array 
    add $t8, $a0, $t8 # $t8 = address temp[k]
    sw $s5, 0($t8) # temp[k] = a[i]
    addi $t3, $t3, 1 # k++
    addi $t1, $t1, 1 # i++
    j while1
while2:
    blt $s2, $t2, SaveBack # go to SaveBack if j >= right
    sll $t7, $t2, 2 #$t7 = j*4
    add $t7, $s1, $t7 # $t7 = address a[j]
    lw $s6, 0($t7) # $s6 = a[j]
    sll $t8, $t3, 2 # $t8 = i*4
    la $a0, TempArray 
    add $t8, $a0, $t8 # $t8 = address temp[k]
    sw $s6, 0($t8) # temp[k] = a[j]
    addi $t3, $t3, 1 # k++
    addi $t2, $t2, 1 # j++
    j while2
    
    #SaveBack back into primitive array
SaveBack:
    or $t1, $zero, $s3 # i = left
    forloop:
    bge $t1, $t3, PrintBounds # if mergeing is done
    sll $t6, $t1, 2 # $t6 = i*4
    add $t6, $s1, $t6 # $t6 = address a[i]
    sll $t8, $t1, 2 # $t6 = i*4
    la $a0, TempArray
    add $t8, $a0, $t8 # $t8 = address temp[i]
    lw $s7, 0($t8) # $s7 = temp[i]
    sw $s7, 0($t6) # store to a[i]
    addi $t1, $t1, 1 # i++
    j forloop
    
PrintBounds:
    li $v0, 4 # system call code for print_str
    la $a0, endl # address of string to print
    syscall
    la $a0, left
    li $v0, 4
    syscall
    move $a0, $s3
    li $v0, 1
    syscall
    la $a0, space
    li $v0, 4
    syscall
    la $a0, mid
    li $v0, 4
    syscall 
    move $a0, $s4
    li $v0, 1
    syscall
    la $a0, space
    li $v0, 4
    syscall
    la $a0, right
    li $v0, 4
    syscall 
    move $a0, $s2
    li $v0, 1
    syscall
    la $a0, endl
    li $v0, 4
    syscall
    
    la $t1, Array #load primitive array's address
    li $t2, 0 #set index = 0
    
PrintArrayOnSorting:
    bge $t2, 20, ExitPrintArrayOnSorting #if count variable > 20, quit printing primitive array
    lw $t3, 0($t1)
    addi $t1, $t1, 4
    move $a0, $t3
    li $v0, 1
    syscall
    la $a0, space
    li $v0, 4
    syscall
    addi $t2, $t2, 1 
    j PrintArrayOnSorting
    
ExitPrintArrayOnSorting:
    la $a0, endl
    li $v0, 4
    syscall

    la $t1, TempArray #load primitive array's address
    li $t2, 0 #set index = 0

PrintTempArrayOnSorting:
    bge $t2, 20, ExitPrintTempArrayOnSorting #if count variable > 20, quit printing temp array
    lw $t3, 0($t1)
    addi $t1, $t1, 4
    move $a0, $t3
    li $v0, 1
    syscall
    la $a0, space
    li $v0, 4
    syscall
    addi $t2, $t2, 1 
    j PrintTempArrayOnSorting
    
ExitPrintTempArrayOnSorting:
    la $a0, endl
    li $v0, 4
    syscall
    
    j ExitMergeSort
    #SaveBack back into primitive array
